module Endpoints where

import Servant ( Get
               , Post
               , Headers
               , Handler
               , err400
               , ServantErr(..)
               , Server
               , OctetStream
               , JSON
               , Header(..)
               , addHeader
               , ReqBody
               , Proxy(..)
               , type (:>)
               , type (:<|>)
               , (:<|>)(..)
               )

import qualified Templates as Templates
import Data.ByteString as BS
import Data.ByteString.Lazy (fromStrict)
import Data.ByteString.Char8
import qualified Data.ByteString.Base64 as B64
import qualified Formats.Srt as Srt
import Data.Text as T
import Subtitle
import Data.Aeson
import Data.Text.Encoding
import Debug.Trace
import Data.Either
import Control.Monad.Error.Class
import Fixer
import Parser
import Control.Monad.IO.Class
import Control.Exception hiding (Handler)
import EndpointData

type ServantType =  "subfixer" :> Get '[Templates.HTMLContent] (Headers '[Header "Cache-Control" String] Templates.HTML)
               :<|> "subfixer" :> ReqBody '[OctetStream] ByteString :> Post '[Templates.HTMLContent] Templates.HTML
               :<|> "subfixer" :> "fixSubtitle" :> ReqBody '[JSON] FixSubtitleRequest :> Post '[OctetStream] ByteString

indexHandler :: Handler (Headers '[Header "Cache-Control" String] Templates.HTML)
indexHandler = return $ addHeader "no-cache" (Templates.index)

throwErrorWithMessage :: String -> Handler a
throwErrorWithMessage msg = throwError $ err400 { errBody = fromStrict $ encodeUtf8 $ T.pack $ msg }

parseBase64Sub :: ByteString -> Handler Subtitle
parseBase64Sub base64Sub = do
  r <- liftIO $ catch (evaluate parse) (\(e::SomeException) -> return $  Left $ displayException e)
  case r of
    Right sub -> return sub
    Left err -> throwErrorWithMessage err
  where
  dropBOM :: ByteString -> ByteString
  dropBOM bs = if BS.take 3 bs == BS.pack [0xef, 0xbb, 0xbf] then BS.drop 3 bs else bs
  parse :: (Either String Subtitle)
  parse = let
    subtitleRaw =
      case B64.decode base64Sub of
        Right srt -> srt
        Left err -> error err
    in Parser.parse $ decodeUtf8 $ dropBOM $ subtitleRaw

subtitleUploadHandler :: ByteString -> Handler Templates.HTML
subtitleUploadHandler base64Sub = do
  subtitle  <- parseBase64Sub base64Sub
  return $ Templates.displaySubtitle subtitle

subtitleFixerHandler :: FixSubtitleRequest -> Handler ByteString
subtitleFixerHandler fsr = do
  subtitle  <- parseBase64Sub $ originalFile fsr
  return $ encodeUtf8 $ fix subtitle (anchor1 $ anchors fsr) (anchor2 $ anchors fsr)

server :: Server ServantType
server = indexHandler :<|> subtitleUploadHandler :<|> subtitleFixerHandler
