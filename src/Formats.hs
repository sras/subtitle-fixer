module Formats where

import Data.Text

data FormatRaw = SRT Text | SSA Text deriving (Show)
