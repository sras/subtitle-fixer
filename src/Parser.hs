module Parser (parse) where

import Data.Attoparsec.Text (choice, parseOnly)
import qualified Formats.Ssa as Ssa 
import qualified Formats.Srt as Srt
import Subtitle
import Data.Text
import Debug.Trace
import Data.ByteString
import Data.Text.Encoding

parse :: Text -> Either String Subtitle
parse raw = parseOnly (choice [Srt.parser, Ssa.parser]) raw
