module EndpointData where

import Data.Text.Encoding
import Subtitle
import Data.Aeson
import Data.ByteString

data Anchors
  = Anchors
      { anchor1 :: Anchor
      , anchor2 :: Anchor
      } deriving (Show)

data FixSubtitleRequest
  = FixSubtitleRequest
      { originalFile :: ByteString
      , anchors :: Anchors
      } deriving (Show)

instance FromJSON FixSubtitleRequest where
  parseJSON (Object v) = do
    file <- encodeUtf8 <$> v .: "file"
    anchors <- v .: "anchors"
    return $ FixSubtitleRequest { originalFile = file, anchors = anchors }

instance FromJSON Anchors where
  parseJSON (Object v) = do
    a1 <- v .: "anchor1"
    a2 <- v .: "anchor2"
    return $ Anchors { anchor1 = a1, anchor2 = a2 }

