module Formats.Ssa (parser) where

import Data.Attoparsec.Text as AP
import Subtitle
import Data.Time.Clock
import Data.Text as T
import Data.Text.Encoding
import Data.Text.Encoding.Error
import Data.Fixed
import Data.ByteString
import Data.List as DL
import Data.Maybe as DM
import qualified Data.Vector as V
import Text.Printf as T
import Formats
import Prelude
import qualified Prelude as P
import Debug.Trace

type DialogueLine = V.Vector Text

data Event = DialogueEvent DialogueLine | OtherEvent Text (V.Vector Text) deriving (Show)

data Ssa = Ssa
  { ssaHead :: !Text
  , ssaTail :: !Text
  , ssaFormat :: ![Text]
  , ssaEvents :: ![Event]
  } deriving (Show)

instance SubtitleRaw Ssa where
  patch = patchSsa
  serialize = serializeSsa

ssaParser :: Parser Ssa
ssaParser = do
    ahead <- manyTill anyChar "[Events]"
    formats <- do
      _ <- many' endOfLine
      _ <- string "Format: "
      sepBy (T.pack <$> many' letter) (string ", ")
    case (DL.elemIndex "Start" formats, DL.elemIndex "End" formats, DL.elemIndex "Text" formats) of
      (Just startIndex, Just endIndex, Just textIndex) -> do
        _ <- many' endOfLine
        events <- many1 (eventParser startIndex endIndex textIndex)
        atail <- many' anyChar
        return $ Ssa { ssaHead = T.pack ahead, ssaTail = T.pack atail, ssaFormat = formats, ssaEvents = events }
      _ -> fail "One of Start, End or Text not found in formats."
  where
  parseTime s = parseOnly timeParser s
  dialogSeg :: Parser Text
  dialogSeg = do
    r <- AP.takeWhile (/= ',')
    _ <- char ','
    return r
  eventParser :: Int -> Int -> Int -> Parser Event
  eventParser si ei ti = do
    eventType <- manyTill (notChar '[') (string ": ")
    dialogItems <- V.fromList <$> AP.count ti dialogSeg -- skip items until text
    text <- takeTill isEndOfLine
    _ <- many1 endOfLine
    return $ if eventType == "Dialogue"
                then DialogueEvent (V.snoc dialogItems text)
                else OtherEvent (T.pack eventType) (V.snoc dialogItems text)

timeParser :: Parser MillSecs
timeParser = do
  hrs <- decimal
  _ <- string ":"
  min <- decimal
  _ <- string ":"
  millisec <- secondParser
  return $ (hrs * 3600 * 1000) + (min * 60 * 1000) + millisec
  where
  secondParser :: Parser Int
  secondParser = do
    p1 <- decimal
    _ <- string "."
    p2 <- decimal
    return $ (p1 * 1000) + (p2 * 10)

parser :: Parser Subtitle
parser = do
  ssa <- ssaParser
  case let formats = ssaFormat ssa  in (DL.elemIndex "Start" formats, DL.elemIndex "End" formats, DL.elemIndex "Text" formats) of
    (Just startIndex, Just endIndex, Just textIndex) -> do
      dialogues <- mapM (getDialogue startIndex endIndex textIndex) (Prelude.zip (ssaEvents ssa) [0..])
      return $ Subtitle { subRaw = ssa, subDialogs = DM.catMaybes dialogues }
    _ -> fail "One of Start, End or Text not found in formats."
  where
    parseTime s = parseOnly timeParser s
    getDialogue :: Int -> Int -> Int -> (Event, Int) -> Parser (Maybe Dialog)
    getDialogue si ei ti (OtherEvent _ _, idx) = return Nothing
    getDialogue si ei ti (DialogueEvent dialogItems, idx) = case (parseTime $ dialogItems V.! si, parseTime $ dialogItems V.! ei) of
      (Right startTime, Right endTime) -> return $ Just $ Dialog
        { idx = idx
        , from = startTime
        , to = endTime
        , dialogs = [dialogItems V.! ti]
        }
      a -> fail $ "Parsing of time string failed :" ++ show (a, dialogItems V.! si, dialogItems V.! ei)

patchSsa ssa sub =
  let
    formats = ssaFormat ssa in 
    case (DL.elemIndex "Start" formats, DL.elemIndex "End" formats) of
      (Just si, Just ei) -> ssa
        { ssaEvents = myZip (patchOne si ei) (ssaEvents ssa) (subDialogs sub) }
  where
    myZip :: (a -> b -> Maybe a)-> [a] -> [b] -> [a]
    myZip !fn [] _ = []
    myZip !fn a [] = a
    myZip !fn (a:xa) (b:xb) = case fn a b of
      Just c -> c:(myZip fn xa xb)
      Nothing -> a:(myZip fn xa (b:xb))
    patchOne :: Int -> Int -> Event -> Dialog -> Maybe Event
    patchOne si ei (OtherEvent _ parts) dialog = Nothing
    patchOne si ei (DialogueEvent parts) dialog =
      let
        newStartTime = mkSsaTime $ from dialog
        newEndTime = mkSsaTime $ to dialog
      in Just $ DialogueEvent $ V.update parts (V.fromList [(si, newStartTime), (ei, newEndTime)])
    mkSsaTime :: Int -> Text
    mkSsaTime i =
      let
        (hrs, mns, mlli) = splitDialogTimeInt i
        secs = div mlli 1000
        hndrth = div (mod mlli 1000) 10
        in T.pack $ T.printf "%02d:%02d:%02d.%02d" hrs mns secs hndrth

serializeSsa a = T.concat
  [ T.unlines $ lines $ ssaHead a
  , T.unlines $
      "[Events]":
      T.concat ["Format: ", T.intercalate ", " $ ssaFormat a]:
      (makeDialogueLine <$> ssaEvents a)
  , T.unlines $ lines $ ssaTail a
  ]
  where
    lines :: Text -> [Text]
    lines a = case parseOnly (many' lineParser)  $ a of
      Right x -> x
      Left _ -> []
      where
        lineParser = do
          r <- AP.takeWhile (not.isEndOfLine)
          _ <- endOfLine;
          return r
    makeDialogueLine :: Event -> Text
    makeDialogueLine (DialogueEvent a) = T.concat ["Dialogue: ",  T.intercalate "," $ V.toList a]
    makeDialogueLine (OtherEvent eventName a) = T.concat [eventName, ": ",  T.intercalate "," $ V.toList a]
