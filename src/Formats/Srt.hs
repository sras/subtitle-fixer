module Formats.Srt (parser) where

import Data.Attoparsec.Text
import Subtitle
import Data.Time.Clock
import Data.Text
import Data.Text.Encoding
import Data.Text.Encoding.Error
import Data.Fixed
import Data.ByteString
import Formats
import Data.Text as T
import Control.Applicative
import Debug.Trace

data Srt = Srt [Dialog] deriving (Show)

instance SubtitleRaw Srt where
  patch a sub = Srt (subDialogs sub)
  serialize = srtSerialize

parser :: Parser Subtitle
parser = do
  dialogs' <- many1' $ do
    indexV <- decimal
    _ <- endOfLine
    fromV <- timeParser
    skipSpace
    _ <- string "-->"
    skipSpace
    toV <- timeParser
    _ <- endOfLine
    subsV <- many' dialogParser
    _ <- many1' endOfLine
    return $ Dialog
      { idx = indexV
      , from = fromV
      , to = toV
      , dialogs = subsV
      }
  let dialogs = reindex dialogs'
  return $ Subtitle { subRaw = Srt $ dialogs, subDialogs = dialogs }

srtSerialize (Srt dialogs_) = T.intercalate "\n\n" $ (toByteString_ <$> dialogs_)
  where
    toByteString_ :: Dialog -> Text
    toByteString_ d = let
      (f_hr, f_min, f_sec) = splitDialogTime $ from d
      (t_hr, t_min, t_sec) = splitDialogTime $ to d
      fromTime = T.intercalate ":" [f_hr, f_min, f_sec]
      toTime = T.intercalate ":" [t_hr, t_min, t_sec]
      in T.intercalate "\n" $ [ T.pack $ show $ idx d
                                            , T.concat [fromTime, " --> ", toTime]
                                            ] Prelude.++ (dialogs d)
timeParser :: Parser MillSecs
timeParser = do
  hrs <- decimal
  _ <- string ":"
  min <- decimal
  _ <- string ":"
  millisec <- secondParser
  return $ (hrs * 3600 * 1000) + (min * 60 * 1000) + millisec
  where
  secondParser :: Parser Int
  secondParser = do
    p1 <- decimal
    _ <- (string "," <|> string ".")
    p2 <- decimal
    return $ (p1 * 1000) + p2

dialogParser :: Parser Text
dialogParser = do
  t <- takeWhile1 (\x -> x /= '\n' && x /= '\r')
  _ <- endOfLine
  return t
