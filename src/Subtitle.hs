{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE StandaloneDeriving #-}

module Subtitle where

import Data.Time.Clock
import Data.Text as T
import Data.Vector
import Text.Printf
import Data.ByteString (ByteString, intercalate)
import Data.Aeson
import Data.Text.Encoding
import Formats
import Data.Attoparsec.Text as AP
import Debug.Trace

class SubtitleRaw a where
  patch :: a -> Subtitle -> a
  serialize :: a -> Text

reindex :: [Dialog] -> [Dialog]
reindex ds = Prelude.zipWith (\i d -> d { idx = i }) [1..] ds

data Anchor =
  Anchor
    { idx_ :: !Int
    , from_ :: !MillSecs
    } deriving (Show)

instance FromJSON Anchor where
  parseJSON (Object v) = do
    idx <- (v .: "idx")
    hrs <- (v .: "hour")
    mins <- (v .: "minute")
    sec <- (v .: "millisecond")
    return $ Anchor { idx_ = idx, from_ = combineDialogTime (hrs, mins, sec) }

data Dialog =
  Dialog
    { idx :: !Int
    , from  :: !MillSecs
    , to  :: !MillSecs
    , dialogs :: ![Text]
    } deriving (Show)

data Subtitle = forall a. (Show a, SubtitleRaw a) => Subtitle { subRaw :: !a, subDialogs :: ![Dialog] }

deriving instance Show Subtitle

type MillSecs = Int

combineDialogTime :: (Text, Text, Text) -> Int
combineDialogTime (hr, min, sec) = let
  mns= 60 * 1000
  hrs = 60 * mns
  sec_cleaned = T.replace "," "" sec
  in ((read.unpack)  hr * hrs) + ((read.unpack) min * mns) + ((read.unpack) sec_cleaned)

splitDialogTimeInt :: Int -> (Int, Int, Int)
splitDialogTimeInt t = let
  mm = 60 * 1000
  totalMins = div t mm
  hrs = div totalMins 60
  mins = mod totalMins 60
  secs = t - (totalMins * mm)
  in (hrs, mins, secs)

splitDialogTime :: Int -> (Text, Text, Text)
splitDialogTime t = let
  (hrs, mins, secs) = splitDialogTimeInt t
  in (formatTime hrs, formatTime mins, formatSeconds secs)
  where
  formatTime :: Int -> Text
  formatTime x = T.pack $ printf "%02d" x
  formatSeconds :: Int -> Text
  formatSeconds s = let
    seconds = div s 1000
    mills = mod s 1000
    in T.pack $ printf "%02d,%03d" seconds mills

fixSubtitle :: Subtitle -> Anchor -> Anchor -> Subtitle
fixSubtitle subtitle a1 a2 = let
  (anchor1, anchor2) = if idx_ a1 < idx_ a2 then (a1, a2) else (a2, a1)
  lookupSource  = (\d -> (idx d, d)) <$> (subDialogs subtitle)
  anchor1Source = case lookup (idx_ anchor1) lookupSource of
    Just x -> x
    Nothing -> error "Cannot find anchor 1 in source"
  anchor2Source = case lookup (idx_ anchor2) lookupSource of
    Just x -> x
    Nothing -> error "Cannot find anchor 1 in source"
  startDelta = (from_ anchor1) - (from anchor1Source)
  endDelta = (from_ anchor2) - (from anchor2Source)
  anchorDistance = (from anchor2Source) - (from anchor1Source)
  deltaChangeRate = (fromIntegral $ endDelta - startDelta :: Double)/(fromIntegral anchorDistance)
  fixDialog :: Dialog -> Dialog
  fixDialog sd = let
    offsetA1 = (from sd) - (from anchor1Source)
    delta = startDelta + (round $ (fromIntegral offsetA1) * deltaChangeRate)
    in sd { from = from sd + delta, to = to sd + delta }
  in traceShow (lookupSource, anchor2Source) $ subtitle { subDialogs = fixDialog <$> (subDialogs subtitle) }
