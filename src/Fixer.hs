module Fixer where

import Subtitle
import Formats.Srt
import Formats.Ssa
import Data.Text as T
import Data.Attoparsec.Text
import Data.ByteString
import Formats

fix :: Subtitle -> Anchor -> Anchor -> Text
fix (sub@Subtitle {subRaw = r}) a1 a2 = serialize $ patch r (fixSubtitle sub a1 a2)
