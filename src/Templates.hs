module Templates where

import Lucid.Html5
import Lucid.Base
import Servant (Accept(..), MimeRender(..))
import Data.Text.Encoding
import Data.Text.Encoding.Error
import Data.FileEmbed
import Data.Text
import Data.Monoid
import Language.Haskell.TH.Syntax
import Subtitle
import Data.List (foldl')
import Text.Printf
import Data.ByteString (ByteString)

data HTMLContent

type HTML = Html ()

instance Accept HTMLContent where
   contentType _ = "text/html"

instance MimeRender HTMLContent (Html ())  where
   mimeRender _ val = renderBS val

common :: HTML -> HTML
common inner = doctypehtml_ $ do
  head_ $ do
    title_ [] "Fix subtitles"
    meta_ [charset_ "utf-8"]
    meta_ [name_ "description", content_ "Fix subtitles that are out of sync due to frame rate difference between the video file and the subtitle."]
    meta_ [name_ "google-site-verification", content_ "aSyqPIXwo-DYJ13T-StyCAuFUvfxccwXvD1Pv2oudBU"]
    link_ [rel_ "canonical", href_ "https://sras.me/subfixer"]
    script_ [src_ "https://code.jquery.com/jquery-3.3.1.min.js"] (mempty :: Text)
    script_ $ decodeUtf8 $ $(embedFile "assets/scripts.js")
    style_ $ ".sub-container { position:relative; top: 10ex; padding: 10px;} .how-to-container { margin-top: -7ex; padding: 35px; ;line-height: 3ex;} \
              \@media only screen and (max-width: 1020px) { .sub-container { position:relative; top: 13ex; }  .how-to-container { margin-top: -10ex; }}\
              \@media only screen and (max-width: 600px) { .sub-container { position:relative; top: 16ex; } .how-to-container { margin-top: -8ex; } }\
              \@media only screen and (max-width: 400) { .sub-container { position:relative; top: 21ex; } .how-to-container { margin-top: -10ex;} }\
              \\
              \.dialog-dialog-container { margin-bottom: 10px; font-size:20px; font-family: sans} .dialog-container {padding: 10px; margin: 10px; margin-bottom: 25px; padding-bottom: 0px; padding-top: 0px; padding-left:20px;border-left: solid 8px #ff9191;} button, input[type=file] { margin-top: 10px;margin-left: 10px; } div {margin: 0px} body {margin: 0px; font-family: arial;} .from-mn, .from-hr, .from-sc { width : 40px; font-size: 20px;} .from-sc { width:80px; font-size:20px;} "
  inner
  div_ [style_ "position:fixed; bottom: 0px; margin-right:10px; margin-bottom: 10px; font-size: 12px; right: 0px;"] $ do
    --let isDirty = if $(gitDirty) then "(dirty)" else "" in
    --  span_ [style_ "color:transparent"] $ toHtml $ (Prelude.take 10 $ $(gitHash))  ++ isDirty ++ " "
    a_ [href_ "https://bitbucket.org/sras/subtitle-fixer"] "Built"
    " with "
    a_ [href_ "https://www.haskell.org/"] "Haskell"
    " and "
    a_ [href_ "http://hackage.haskell.org/package/servant"] "Servant"
    " by "
    a_ [href_ "https://sras.me"] "sras"

displaySubtitle :: Subtitle -> HTML
displaySubtitle sub = case subDialogs sub of
    [] -> "Cannot read subtitles. Please check if the file's encoding is UTF-8 or ASCII."
    _ -> Data.List.foldl' (\a b -> a <> displayDialog b) mempty (subDialogs sub)
  where
  displayDialog :: Dialog -> HTML
  displayDialog dialog = div_ [class_ "dialog-container"] $ do
    input_ [type_ "hidden", class_ "idx", value_ (pack $ show $ idx dialog)]
    div_ [class_ "dialog-dialog-container"] $ Data.List.foldl' (\a b -> a <> div_ [] (toHtml b)) mempty (dialogs dialog)
    span_ [] $ Data.List.foldl' (\a (c, b) -> a <> input_ [type_ "hidden", class_ $ pack $ printf "src-dialog-%d" c, value_  b]) mempty (Prelude.zip [(0::Int)..] (dialogs dialog))
    let
      (hr, mn, sc) = splitDialogTime $ from dialog
      in span_ [] $ do
        input_ [class_ "from-hr", placeholder_ hr]
        span_ [] $ ":"
        input_ [class_ "from-mn", placeholder_ mn]
        span_ [] $ ":"
        input_ [class_ "from-sc", placeholder_ sc]

index :: HTML
index = common $ div_ [] $ do
  div_ [style_ "position:fixed;z-index: 1000;top: 0px; padding-bottom: 10px; background-color: #ffa229e6; width: 100%;"] $ do
    input_ [id_ "subtitle-file", type_ "file"]
    button_ [id_ "upload-button"] "Upload subtitle file"
    div_ [] $ button_ [id_ "fix-button"] "Fix subtitle"
  div_ [class_ "sub-container", id_ "sub-container"] $ do
    div_ [class_ "how-to-container"] $ do
      h3_ [] "How to use?"
      span_ [] $ do
        "Upload your subtitle file in "
        a_ [href_ "https://en.wikipedia.org/wiki/SubRip"] "SubRip(.srt)"
        " or "
        a_ [href_ "https://www.matroska.org/technical/specs/subtitles/ssa.html"] "SSA"
        " format using the file selector above. "
        b_ [] "The file should be under 1 Mb in size and should be encoded in a UTF-8 or ASCII encoding. "
        "If successful, the subtitles will be shown here. For each dialogue in the subtitle, there will be input fields where you can fill in the actual time when that particular dialogue occur in the movie. Fill in timings for any two dialogue. For better accuracy pick one near the start of the movie, and one near the end. Then press the \"Fix subtitles\" button. The fixed subtitle will be available as a download"
  a_ [id_ "hidden-link", href_ "#"] $ mempty
