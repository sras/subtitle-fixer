var upload_file_raw = null;
var upload_file_name = null;

function isNotEmpty(s) {
  return !(s === undefined || s === null || s == "");
}

function getTiming(e) {
  var hr_s = $('.from-hr', e).val();
  var mn_s = $('.from-mn', e).val();
  var sec_s = $('.from-sc', e).val();
  var idx_s = $('.idx', e).val();
  if (isNotEmpty(hr_s) && isNotEmpty(mn_s) && isNotEmpty(sec_s) && isNotEmpty(idx_s)) {
    return {idx : parseInt(idx_s), hour : hr_s, millisecond : sec_s, minute: mn_s };
  } else {
    return null;
  }
}

$(function() {
  $('#fix-button').hide();

  $('#how-to').on('click', function()  {
    alert("After uploading subtitles, in the shown form, enter the actual time when any two dialogs appear in the movie file and press 'Fix Subtitles' button. For better accuaracy, pick one dialog near the start and one near the end of the movie.");

  });

  $('#fix-button').on('click', function()  {
    var anchor_1 = null;
    var anchor_2 = null;
    var found_flag = false;
    $('.dialog-container').each(function(c, dc) {
      var t = getTiming($(dc));
      if (t !== null) {
        if (anchor_1) {
          var anchors = {anchor1 : anchor_1, anchor2 : t};
          found_flag = true;
          $.ajax({ data: JSON.stringify({file: upload_file_raw, anchors: anchors})
                 , 'contentType': 'application/json; charset=utf-8'
                 , 'url': '/subfixer/fixSubtitle'
                 , 'type': 'POST'
                 , 'error': function(x, s, m) { alert(s + ":" + m + "," + x.responseText); }
                 , 'success': function(data) {
                      var anchor = $('#hidden-link');  
                      anchor.attr('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(data));
                      if (upload_file_name) {
                        var parts = upload_file_name.split('.');
                        if (parts.length > 1) {
                          parts.splice(parts.length - 1, 0, "RESYNCED")
                          anchor.attr('download', parts.join("."));
                        } else {
                          anchor.attr('download', upload_file_name);
                        }
                      } else {
                        anchor.attr('download', "sub-fixed");
                      }
                      anchor.get(0).click();
                   }
                 });
          return false;
        } else {
          anchor_1 = t;
        }
      }
    });
    if (!found_flag) {
      alert("Please enter actual time (time when it appear in the movie file that you have) for any two dialogues");
    }
  });

  $('#upload-button').on('click', function (e) {
    upload_file_name = $('#subtitle-file').get(0).files[0].name;
    var fileReader = new FileReader();
    fileReader.onload = function (e) {
      var data = fileReader.result.substr(fileReader.result.indexOf(',') + 1);  // data <-- in this var you have the file data in Base64 format
      upload_file_raw = data;
      $.ajax('/subfixer', {
          data : data,
          type: 'POST',
          'processdata': false,
          'contentType': 'application/octet-stream',
          'error': function(x, s, m) {  alert(s + ":" + m + "," + x.responseText); },
          'success': function(data) {
            $('#sub-container').html(data);
            if($('.dialog-container').length > 0) {
              $('#fix-button').show();
            }
          }
      });
    };
    fileReader.readAsDataURL($('#subtitle-file').prop('files')[0]);
  });
})


