module Main where

import Servant ( Get
               , Post
               , OctetStream
               , Headers
               , JSON
               , Header(..)
               , addHeader
               , ReqBody
               , Proxy(..)
               , type (:>)
               , type (:<|>)
               , (:<|>)(..)
               )
import Servant.Server (Handler, Server, Application, serve)
import Network.Wai.Handler.Warp (run)
import Control.Monad.IO.Class (liftIO)
import Data.Text as T
import qualified Templates as Templates
import Data.ByteString
import Data.ByteString.Char8
import qualified Data.ByteString.Base64 as B64
import qualified Formats.Srt as Srt
import qualified Endpoints as EP

app :: Application
app = serve (Proxy :: Proxy EP.ServantType) EP.server

main :: IO ()
main = run 4000 app
